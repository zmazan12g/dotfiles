# vim:foldmethod=marker

# If not running interactively, do nothing.
status is-interactive; or return

source ~/.config/fish/rc.d/opts.fish
source ~/.config/fish/rc.d/binds.fish

source ~/.config/fish/rc.d/funcs.fish
source ~/.config/fish/rc.d/alias/app.fish
source ~/.config/fish/rc.d/alias/git.fish

source ~/.config/fish/rc.d/startup.fish
