# Fish Configurations

All my Fish configuration scripts are here, which are then sourced by
[`~/.config/config.fish`](./config.fish).
