# vim:foldmethod=marker

#: lf {{{
function lfcd --wraps="lf" --description="lf - Terminal file manager (changing directory on exit)"
	cd "$(command lf -print-last-dir $argv)"
end
#: }}}

#: yazi {{{
function yy --wraps="yazi" --description="yazi - Terminal file manager (changing directory on exit)"
	set __tmp "$(mktemp -t "yazi-cwd.XXXXXX")"

	yazi $argv --cwd-file="$__tmp"

	set __cwd "$(cat -- "$__tmp")"

	if [ -n "$__cwd" ]; and [ "$__cwd" != "$PWD" ]
		cd -- "$__cwd"
	end

	rm -f -- "$__tmp"
end
#: }}}
