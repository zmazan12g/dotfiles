# Bash Configurations

All my Bash configuration scripts are here, which are then sourced by
[`~/.bash_profile`](../.bash_profile) and [`~/.bashrc`](../.bashrc).
