# vim:foldmethod=marker

#: lf {{{
function lfcd() { cd "$(command lf -print-last-dir "${@}")"; }
#: }}}

#: yazi {{{
function yy() {
	local __cwd
	local __tmp
	__tmp="$(mktemp -t "yazi-cwd.XXXXXX")"

	yazi "${@}" --cwd-file="${__tmp}"

	__cwd="$(cat -- "${__tmp}")"

	if [[ -n "${__cwd}" ]] && [[ "${__cwd}" != "${PWD}" ]]; then
		cd -- "${__cwd}"
	fi

	rm -f -- "${__tmp}"
}
#: }}}
