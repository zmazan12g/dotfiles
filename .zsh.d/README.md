# ZSH Configurations

All my ZSH configuration scripts are here, which are then sourced by
[`~/.zprofile`](../.zprofile) and [`~/.zshrc`](../.zshrc).
